#include "function.h"



GLfloat		Matrices[4][16];
GLfloat		vCamera[3][3];

GLuint		GLSL[3];

GLuint		uMatLoc[5];
GLfloat		sTok = 0.0f;


void GLInit(void){
	  
	if( glfwInit() != GL_TRUE)				
	Shut_Down(1);
	
	if( glfwOpenWindow( window_width, window_height, 8, 8, 8, 0, 32, 8, GLFW_WINDOW) != GL_TRUE)
	Shut_Down(1);
 	
	glfwSetWindowTitle("The GLFW Window");
		
	printf("GL VENDOR:---  %s \n",		glGetString(GL_VENDOR));
	printf("GL RENDERER:-  %s \n",		glGetString(GL_RENDERER));
	printf("GL VERSION:--  %s \n",  	glGetString(GL_VERSION));
	printf("GL SHADING:--  %s \n",		glGetString(GL_SHADING_LANGUAGE_VERSION));


	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	//~ glDisable(GL_CULL_FACE);    
	//~ glFrontFace(GL_CW);

	MLoadIdentity(Matrices[0]);
	MLoadIdentity(Matrices[1]);
	MLoadIdentity(Matrices[2]);
	
	float aspect_ratio = (float)window_height / window_width;
	_MFrustum( Matrices[0], 1.0f, -1.0f, -1.0f * aspect_ratio, 1.0f * aspect_ratio, 2.0f, 1000.0f);	

	vCamera[0][0] = 0.0f; vCamera[0][1] = 0.0f; vCamera[0][2] =-6.0f;	// Pose
	vCamera[1][0] = 0.0f; vCamera[1][1] = 0.0f; vCamera[1][2] = 0.0f;	// View
	vCamera[2][0] = 0.0f; vCamera[2][1] = 1.0f; vCamera[2][2] = 0.0f;	// UpVx

	_LookAtM( Matrices[1], vCamera[0], vCamera[1], vCamera[2]);
	_MTranslate( Matrices[2], 0.0f, -35.0f, -125.0f);
	
	Shader_Setup();

	GLSL[0]		= glCreateProgram();
	GLSL[1]		= glCreateShader(GL_VERTEX_SHADER);
	GLSL[2]		= glCreateShader(GL_FRAGMENT_SHADER);
	
	Read_Shader( GLSL[0], GLSL[1], "glsl/vShader.glsl");
	Read_Shader( GLSL[0], GLSL[2], "glsl/fShader.glsl");
	
	glLinkProgram( GLSL[0]);
	
	GLint prog_link_success;
	glGetObjectParameterivARB( GLSL[0], GL_OBJECT_LINK_STATUS_ARB, &prog_link_success);

	if(!prog_link_success) {
		printf("\nThe shaders could not be linked\n");
		print_program_info_log( GLSL[0]);
	}else{
		printf("\nShaders Succesfully Created And Linked\n");
	}	
}

void GameInit(){
	
	LoadModel("Data/model.ms3d");	
}


void Shut_Down(GLint return_code){
	
	glDeleteProgram( GLSL[0]);
	glDeleteShader( GLSL[1]);
	glDeleteShader( GLSL[2]);		
	
	DestroyModel();
		
	glfwTerminate();
	exit(return_code);
}



void ActivateClientState(){
	glEnableClientState( GL_VERTEX_ARRAY );
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );
	glEnableClientState( GL_NORMAL_ARRAY);	
}


void DeactivateClientState(){
    glDisableClientState( GL_VERTEX_ARRAY );
	glDisableClientState( GL_TEXTURE_COORD_ARRAY );
	glDisableClientState( GL_NORMAL_ARRAY);		
}


void Main_Loop(void){

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor( 0.5, 0.5, 0.5, 0.5);

	sTok += 0.25f;
	glUseProgram( GLSL[0]);
	_MRotate( Matrices[3], sTok, 0, 1, 0); 

	uMatLoc[0]					= glGetUniformLocation( GLSL[0], "uForm");
	glUniform1f( uMatLoc[0], sTok);

	uMatLoc[1]					= glGetUniformLocation( GLSL[0], "uView_Matrix");
	uMatLoc[2]					= glGetUniformLocation( GLSL[0], "uProj_Matrix");
	uMatLoc[3]					= glGetUniformLocation( GLSL[0], "Model_Matrix");
	uMatLoc[4]					= glGetUniformLocation( GLSL[0], "Rotation");
			
	glUniformMatrix4fv( uMatLoc[1], 1, GL_FALSE, Matrices[1]);
	glUniformMatrix4fv( uMatLoc[2], 1, GL_FALSE, Matrices[0]);
	glUniformMatrix4fv( uMatLoc[3], 1, GL_FALSE, Matrices[2]);
	glUniformMatrix4fv( uMatLoc[4], 1, GL_FALSE, Matrices[3]);
	
	ActivateClientState();
	
	RenderMeshArray(0);
	RenderMeshArray(1);

	DeactivateClientState();
	
	glfwSwapBuffers();
}
