#version 130


in vec3				  vPose;


uniform float 		uForm;

uniform mat4		uProj_Matrix;
uniform mat4		uView_Matrix;
uniform mat4		Model_Matrix;
uniform mat4		Rotation;


void main () {
	float PI			= 3.14159265358979323846264;
	float angle			= 27.0;
	//~ float rad_angle		= uForm * PI/180.0;

	//~ d.x = c.x*cos(rad_angle) - c.y*sin(rad_angle);
	//~ d.y = c.y*cos(rad_angle) + c.x*sin(rad_angle);		
										

	gl_Position			= (uView_Matrix * uProj_Matrix) * (Model_Matrix * Rotation) * vec4(vPose, 1.0);
}
