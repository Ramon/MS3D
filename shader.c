#include "function.h"





void	print_shader_info_log( GLuint shader_index){
	int max_length = 2048;
	int actual_length = 0;
	char log[2048];
	
	glGetShaderInfoLog (shader_index, max_length, &actual_length, log);
	printf ("shader info log for GL index %u:\n%s\n", shader_index, log);
}


void print_program_info_log( GLuint program) {
	int max_length = 2048;
	int actual_length = 0;
	char log[2048];
	
	glGetProgramInfoLog (program, max_length, &actual_length, log);
	printf ("program info log for GL index %u:\n%s", program, log);
}


void Shader_Setup(){
	if(glewInit() != GLEW_OK)
	printf("glewInit not supported");

	if(GLEW_ARB_vertex_shader && GLEW_ARB_fragment_shader){
		printf("\n\nReady for GLSL \n");				
	}else {
		printf("\nNot totally ready for GLSL ;_; >> GLEW_ARB_vertex_shader && GLEW_ARB_fragment_shader ");
	}
	
	if(GLEW_ARB_vertex_program)
	printf("Status: ARB vertex programs available.\n");

	if(glewGetExtension("GL_ARB_fragment_program"))
	printf("Status: ARB fragment programs available.\n");

	if(glewIsSupported("GL_VERSION_1_4  GL_ARB_point_sprite"))
	printf("Status: ARB point sprites available.\n");		
	printf("\n");		
}


int Read_Shader( unsigned int program, unsigned int vShader, const char * path){
	
	char * vv	= ReadFile(path);
	int params;	
	
	glShaderSource( vShader, 1, (const GLchar **)&vv, 0);
	glCompileShader( vShader);
	params	= -1;
	
	glGetShaderiv( vShader, GL_COMPILE_STATUS, &params);		
		
	if(GL_TRUE != params) {
		printf("Error Compiling: %s\n", path);
		print_shader_info_log( vShader);
		
		return params;
	}
	glAttachShader( program, vShader);		
	free(vv);
	
	return params;
}
