#include "function.h"




void MoveToArrays( int Verts, int Trits, int i){
	memcpy( Array_Vertices[Verts].Vec,		Vertices[Verts].Vertex,				sizeof(VECTOR3D));
	memcpy( Array_Normals[Verts].Vec,		Triangles[Trits].VertexNormals[i],	sizeof(VECTOR3D));
	
	Array_TexCoords[Verts].Vec[0]	= Triangles[Trits].S[i];
	Array_TexCoords[Verts].Vec[1]	= Triangles[Trits].T[i];
	Array_BoneIDs[Verts]			= Vertices[Verts].BoneId;	
}


unsigned short unSortFunction(int flag, unsigned short array_size){
		
	unsigned short		nNumVertices = NumElements[nVertex];
	int					nNumTriangles = 0;
	float				Coords[2];
	int					material, Verts, Trits, i;
	
	if(flag > 0 && array_size > 0){
		Array_Vertices	=	(VECTOR3D *)malloc(array_size * sizeof(VECTOR3D));
		Array_Normals	=	(VECTOR3D *)malloc(array_size * sizeof(VECTOR3D));
		Array_TexCoords	=	(VECTOR2D *)malloc(array_size * sizeof(VECTOR2D));
		Array_BoneIDs	=	(char *)	malloc(array_size * sizeof(char));
	}
	
	for(Verts = 0; Verts < NumElements[nVertex]; Verts++){
		
		nNumTriangles = 0;
		for( Trits = 0; Trits < NumElements[nTriangle]; Trits++){
			for( i = 0; i < 3; i++){
				if(Verts == Triangles[Trits].vIndices[i]){
								
					nNumTriangles++;
					
					if(nNumTriangles == 1){
						Coords[0]	= Triangles[Trits].S[i];
						Coords[1]	= Triangles[Trits].T[i];
						material	= Meshes[Triangles[Trits].GroupIndex].MaterialIndex;

						if(flag > 0 && array_size > 0){
							MoveToArrays( Verts, Trits, i);
						}
					}
					else{
						if((Triangles[Trits].S[i] != Coords[0] || Triangles[Trits].T[i] != Coords[1]) &&
						   (Meshes[Triangles[Trits].GroupIndex].MaterialIndex == material)){
							   
							if(flag > 0 && array_size > 0){
								MoveToArrays( Verts, Trits, i);
							}   
							nNumVertices++;
					   }		
					}	
				}			
			}
		}
	}
	
	return nNumVertices;
}


void DestroyModel(){

	if( Array_Vertices){		free(Array_Vertices);	Array_Vertices	= NULL;}
	if( Array_Normals){			free(Array_Normals);	Array_Normals	= NULL;}
	if( Array_TexCoords){		free(Array_TexCoords);	Array_TexCoords	= NULL;}
	if( Array_BoneIDs){ 		free(Array_BoneIDs);	Array_BoneIDs	= NULL;}

	if(Mesh_Indices){
		int m;
		for(m = 0; m < NumElements[nMesh]; m++){
			free(Mesh_Indices[m]);
		}
		free(Mesh_Indices);
	}
		
	if(NumElements[nVertex]){	NumElements[nVertex]	= 0; free( Vertices);	Vertices	= NULL;}	
	if(NumElements[nTriangle]){	NumElements[nTriangle]	= 0; free( Triangles);	Triangles	= NULL;}
	if(NumElements[nMaterial]){	NumElements[nMaterial]	= 0; free( Materials);	Materials	= NULL;}
	
	if(NumElements[nMesh]){
		int index;
		for(index = 0; index < NumElements[nMesh]; index++){
			free( Meshes[index].TrianglesIndices);
		}
		NumElements[nMesh] = 0;
		free(Meshes);
	}
	
}


void RenderMeshArray(int i){			
	glVertexPointer( 3, GL_FLOAT, 0,	Array_Vertices->Vec);	
	glNormalPointer( GL_FLOAT, 0,		Array_Normals->Vec);
	glTexCoordPointer( 2, GL_FLOAT, 0,	Array_TexCoords->Vec);
	
	glDrawElements(GL_TRIANGLES, Meshes[i].NumTriangles * 3, GL_UNSIGNED_SHORT, Mesh_Indices[i]);	
}
