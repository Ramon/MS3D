#include "function.h"


unsigned short		NumElements[4];

MS3D_VERTEX			* 	Vertices;
MS3D_TRIANGLE		* 	Triangles;
MS3D_MESH			* 	Meshes;
MS3D_MATERIAL		* 	Materials;

unsigned short		**	Mesh_Indices;

VECTOR3D			*	Array_Vertices;
VECTOR3D			*	Array_Normals;
VECTOR2D			*	Array_TexCoords;
char				*	Array_BoneIDs;



int LoadHeader(FILE * File){

	MS3D_HEADER Header;

	fread( Header.Id, sizeof(char), 10, File);
	fread(&Header.Version, 1, sizeof(int), File);

	if(strncmp( Header.Id, "MS3D000000", 10 ))       return 0;
	if(Header.Version != 3 && Header.Version != 4)   return 0;
	
	return 1;
}


void LoadVertices(FILE * File){

	fread(&NumElements[nVertex], sizeof(unsigned short), 1, File);
	Vertices = (MS3D_VERTEX *)malloc(NumElements[nVertex] * (sizeof(MS3D_VERTEX)));
	printf("\n Vertices: %i", NumElements[nVertex]);
	int Index;
	for(Index = 0;Index < NumElements[nVertex];Index++){

		fread(&Vertices[Index].Flags,    sizeof(unsigned char), 1, File);
		fread( Vertices[Index].Vertex,   sizeof(float[3]),		1, File);
		fread(&Vertices[Index].BoneId,   sizeof(char),			1, File);
		fread(&Vertices[Index].Refcount, sizeof(unsigned char), 1, File);
	} 
}


void LoadTriangles(FILE * File){

	fread(&NumElements[nTriangle], sizeof(unsigned short), 1, File);
	Triangles = (MS3D_TRIANGLE *)malloc(NumElements[nTriangle] * (sizeof(MS3D_TRIANGLE)));
	printf("\n Triangles: %i", NumElements[nTriangle]);
	int Index;
	for(Index = 0;Index < NumElements[nTriangle];Index++){
				
		fread(&Triangles[Index].Flags,            sizeof(unsigned short),	1, File);
		fread( Triangles[Index].vIndices,		  sizeof(unsigned short),	3, File);
	  
		fread( Triangles[Index].VertexNormals, sizeof(float),				9, File);

		fread( Triangles[Index].S,                sizeof(float),			3, File);
		fread( Triangles[Index].T,                sizeof(float),			3, File); 
		
		Triangles[Index].T[0] = -Triangles[Index].T[0];
		Triangles[Index].T[1] = -Triangles[Index].T[1];
		Triangles[Index].T[2] = -Triangles[Index].T[2];
				
		fread(&Triangles[Index].SmoothingGroup,   sizeof(unsigned char),  1, File);
		fread(&Triangles[Index].GroupIndex,       sizeof(unsigned char),  1, File);
	} 
}


void LoadMeshes(FILE * File){

	fread(&NumElements[nMesh], sizeof(unsigned short), 1, File);
	
	Meshes = (MS3D_MESH *)malloc(NumElements[nMesh] * (sizeof(MS3D_MESH)));
	printf("\n Meshes: %i", NumElements[nMesh]);
	int Index;
	
	for(Index = 0;Index < NumElements[nMesh];Index++){
		fread(&Meshes[Index].Flags,            sizeof(unsigned char),  1, File);
		fread( Meshes[Index].Name,             sizeof(char),          32, File);
		fread(&Meshes[Index].NumTriangles,     sizeof(unsigned short), 1, File);

		Meshes[Index].TrianglesIndices = (unsigned short *)malloc(Meshes[Index].NumTriangles * (sizeof(unsigned short)));
		
		fread( Meshes[Index].TrianglesIndices, sizeof(unsigned short), Meshes[Index].NumTriangles, File);
		fread(&Meshes[Index].MaterialIndex,    sizeof(char),           1, File);
	}
}


void LoadMaterials(FILE * File){

	fread(&NumElements[nMaterial], sizeof(unsigned short), 1, File);

	Materials = (MS3D_MATERIAL *)malloc(NumElements[nMaterial] * (sizeof(MS3D_MATERIAL)));
	printf("\n Materials: %i", NumElements[nMaterial]);
	int Index;
	
	for(Index = 0;Index < NumElements[nMaterial];Index++) {
		fread( Materials[Index].Name,         sizeof(char), 32, File);
		fread( Materials[Index].Ambient,      sizeof(float), 4, File);
		fread( Materials[Index].Diffuse,      sizeof(float), 4, File);
		fread( Materials[Index].Specular,     sizeof(float), 4, File);
		fread( Materials[Index].Emissive,     sizeof(float), 4, File);
		fread(&Materials[Index].Shininess,    sizeof(float), 1, File);
		fread(&Materials[Index].Transparency, sizeof(float), 1, File);
		fread(&Materials[Index].Mode,         sizeof(char),  1, File);
		fread( Materials[Index].Texture,      sizeof(char), 128, File);
		fread( Materials[Index].AlphaMap,     sizeof(char), 128, File); 
		
		printf(" \nTexture image: %s", Materials[Index].Name); 
	}
}


void LoadModel(const char * path){
		
	FILE * File;
	File = fopen( path, "rb");
	
	if(!File){
		printf(" ERROR!!! Can't open File, please check if for case sensitive names/paths");
		fclose(File);
	}else{
		printf("\n");
		
		if( LoadHeader(File) == 0){
			printf(" error loading header\n");
		}else{
			printf(" success loading header\n");			
		}
		
		LoadVertices(File);
		LoadTriangles(File);
		LoadMeshes(File);
		LoadMaterials(File);
		fclose(File);
		
		unsigned short size = unSortFunction(0, 0);
		printf("\n unsorted array Size: %i", unSortFunction(0, 0));
		
		unSortFunction( 1, size);
		
		Mesh_Indices = malloc(NumElements[nMesh]*sizeof(unsigned short*));
		int m, u;
		for( m = 0; m < NumElements[nMesh]; m++){
			Mesh_Indices[m] = (unsigned short *)malloc((Meshes[m].NumTriangles * 3) * sizeof(unsigned short));
			
			for( u = 0; u < Meshes[m].NumTriangles; u++){
				
				MS3D_TRIANGLE * Tri = &Triangles[Meshes[m].TrianglesIndices[u]];
				
				Mesh_Indices[m][(u*3)+0] = Tri->vIndices[0];
				Mesh_Indices[m][(u*3)+1] = Tri->vIndices[1];
				Mesh_Indices[m][(u*3)+2] = Tri->vIndices[2];		
			}
		}
		
		printf("\n Close File");
	}
}
