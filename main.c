#include "function.h"


//~ gcc -Wall -c "%f" function.c matrix.c shader.c
//~ gcc -Wall -o "%e" "%f" function.c matrix.c shader.c -lm -lGLEW -lGL -glfw


int main( int arg, char * argv[]){
	
	GLInit();
	GameInit();
	
	while(1){
		if(glfwGetKey(GLFW_KEY_ESC) == GLFW_PRESS)
		break;
	
		Main_Loop();
	}

	Shut_Down(0);
	
	return 0;
}

