#include "function.h"
#include "ms3d.h"


char * ReadFile(const char * path){
	FILE *	tFile;
	char *	buffer;
	int		lSize;
	
	tFile = fopen( path, "rt");
	if(tFile){
		
		fseek(tFile , 0 , SEEK_END);
		lSize = ftell (tFile);
		rewind (tFile);		
		
		buffer = (char*) malloc (sizeof(char)*(lSize +1));
		fread( buffer, 1, lSize, tFile);
		buffer[lSize] = '\0';

		fclose(tFile);		
		
	}else{
		printf("\n\nError at opening file: %s\n", path);
	}

	return buffer;
}

