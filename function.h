#ifndef __FUNCTION_H
#define __FUNCTION_H

#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GL/glfw.h>

#include "matrix.h"
#include "ms3d.h"

#define window_width	800
#define window_height	600

#define		nVertex			0
#define 	nTriangle		1
#define		nMesh			2
#define		nMaterial		3

extern unsigned short		NumElements[4];

extern MS3D_VERTEX		* 	Vertices;
extern MS3D_TRIANGLE	* 	Triangles;
extern MS3D_MESH		* 	Meshes;
extern MS3D_MATERIAL	* 	Materials;


extern unsigned short	**	Mesh_Indices;

extern VECTOR3D			*	Array_Vertices;
extern VECTOR3D			*	Array_Normals;
extern VECTOR2D			*	Array_TexCoords;
extern char				*	Array_BoneIDs;


void				GLInit(void);
void				GameInit(void);

void				Shut_Down(GLint);
void				Main_Loop(void);

void				LoadModel(const char *);
void				DestroyModel(void);

void				RenderMeshArray(int);
unsigned short		unSortFunction(int, unsigned short);

void				Shader_Setup();

void				print_program_info_log( GLuint);
void				print_shader_info_log(GLuint);

int					Read_Shader( GLuint, GLuint, const char *);

char			 * 	ReadFile(const char *);

#endif
